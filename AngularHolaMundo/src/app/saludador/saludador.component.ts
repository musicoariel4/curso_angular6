import { Component, OnInit } from '@angular/core';


@Component({
  selector: 'app-saludador',
  templateUrl: './saludador.component.html',
  styleUrls: ['./saludador.component.css']
})
export class SaludadorComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }
  
  contador = 1;

  incrementar() {
    this.contador++;
  }

  decrementar() {
  this.contador--;}

}
export class AppComponent {
  nombre='';
  apellido='';
}
