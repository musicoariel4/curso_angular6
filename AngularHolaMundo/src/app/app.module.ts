import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {FormsModule} from '@angular/forms';//Cuando utilizamos la directiva ngModel debemos importar la clase 'FormsModule' 

import { AppComponent } from './app.component';
import { SaludadorComponent } from './saludador/saludador.component';

@NgModule({
  declarations: [
    AppComponent,
    SaludadorComponent
  ],
  imports: [
    BrowserModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
