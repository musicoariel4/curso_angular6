import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { DestinoViajeComponent } from './destino-viaje/destino-viaje.component';
import { ListaDestinosComponent } from './lista-destinos/lista-destinos.component';
import { Routes, RouterModule } from '@angular/router';
import { DestinoDetalleComponent } from './destino-detalle/destino-detalle.component';
//import { HeroesComponent } from './heroes/heroes.component';

const routes: Routes = [

  {path: '',redirectTo:'home',pathMatch:'full'},
  
  {path:'home',component:ListaDestinosComponent},  
  {path:'destino',component:DestinoDetalleComponent}
  ];

@NgModule({
  declarations: [
    AppComponent,
    DestinoViajeComponent,
    ListaDestinosComponent,
    DestinoDetalleComponent,
   // HeroesComponent,
   
   
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    RouterModule.forRoot(routes)
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
