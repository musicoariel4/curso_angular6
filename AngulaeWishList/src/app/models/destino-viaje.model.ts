
//modelo del dominio
export class DestinoViaje{

  /*  nombre:string;
       imagenUrl: string;
        constructor (n:string,u:string){
                 this.nombre =n;
               this.imagenUrl= u; 
    }*/
    
    ///****esto hace lo mismo en dforma abreviada. sin declarar variable
     // variable bandera
     private selected: boolean;
     
     public  servicios : string[];
    constructor(public nombre: string, public u : string){
            this.servicios=['almuerzo','desayuno'];
        }
      // metodo para identificar si esta selecionado (si o no)
      isSelected():boolean{
          return this.selected;
      }
      // metodo para marcar como selecionado 
       setSelected(s:boolean){
           this.selected= true;
      }
}       
