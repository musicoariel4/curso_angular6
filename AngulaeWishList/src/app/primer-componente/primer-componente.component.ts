import { Component   } from '@angular/core';

@Component({
  selector: 'hello-world',
  templateUrl: './primer-componente.component.html',
  styleUrls: ['./primer-componente.component.css']
})

 export class HelloWorld  {

    variable = "angular ariel";

 }
